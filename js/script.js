function changeDisplay() {
    for (let index = 0; index < document.getElementsByClassName('qte').length; index++) {
        if (document.getElementsByClassName('qte')[index].style.display == "none") {
            document.getElementsByClassName('qte')[index].style.display = "";
        } else {
           document.getElementsByClassName('qte')[index].style.display = "none";
        }
    }
}

function disap() {
    for (let index = 0; index < document.getElementsByClassName('qte').length; index++) {
        document.getElementsByClassName('qte')[index].style.display = "none";
    }
}

function decrease(e) {
    if (document.getElementById(e).innerHTML > 0) {
        document.getElementById(e).innerHTML = document.getElementById(e).innerHTML-1;
    }    
}

function increase(e,v) {
    if (document.getElementById(e).innerHTML < parseInt(document.getElementsByClassName("qte")[v].innerHTML)) {
        document.getElementById(e).innerHTML = parseInt(document.getElementById(e).innerHTML)+1;
    }    
}

function envoiForm() {
    let cptF = 0;
    if(document.getElementById('prenom').value == "") {
        document.getElementById('sansPrenom').innerHTML = "Prénom vide";
        document.getElementById('prenom').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansPrenom').innerHTML = "";
        document.getElementById('prenom').style.border = "none";
    }

    if(document.getElementById('nom').value == "") {
        document.getElementById('sansNom').innerHTML = "Nom vide";
        document.getElementById('nom').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansNom').innerHTML = "";
        document.getElementById('nom').style.border = "none";
    }

    if(document.getElementById('mail').value == "") {
        document.getElementById('sansMail').innerHTML = "Mail vide";
        document.getElementById('mail').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansMail').innerHTML = "";
        document.getElementById('mail').style.border = "none";
    }

    if(document.getElementById('ddn').value == "") {
        document.getElementById('sansDate').innerHTML = "Date vide";
        document.getElementById('ddn').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansDate').innerHTML = "";
        document.getElementById('ddn').style.border = "none";
    }

    if(document.getElementById('sujet').value == "") {
        document.getElementById('sansSujet').innerHTML = "Sujet vide";
        document.getElementById('sujet').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansSujet').innerHTML = "";
        document.getElementById('sujet').style.border = "none";
    }

    if(document.getElementById('contenuMail').value == "") {
        document.getElementById('sansContenu').innerHTML = "Contenu vide";
        document.getElementById('contenuMail').style.border = "3px solid red";
        cptF++;
    }else {
        document.getElementById('sansContenu').innerHTML = "";
        document.getElementById('contenuMail').style.border = "none";
    }

    return cptF == 0;
}

function imageZoom(image){
    document.getElementById(image).style.position = "absolute";
    document.getElementById(image).style.border = "3px solid #112d4e";
    document.getElementById(image).style.borderRadius = "30px";
    document.getElementById(image).style.right = "20px";
    document.getElementById(image).height *= 3;
    document.getElementById(image).width *= 3;
}

function imageDezoom(image){
    document.getElementById(image).style.position = "static";
    document.getElementById(image).style.border = "none";
    document.getElementById(image).style.borderRadius = "0";
    document.getElementById(image).height /= 3;
    document.getElementById(image).width /= 3;
}

function ajoutPanier(produit){

    var qte = document.getElementById(produit).innerHTML;
    if (qte >= 0) {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
               
            }
        };
        xhttp.open("POST","traitementPanier.php",true);
        xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhttp.send("qte="+qte+"&id="+produit);
    }
}
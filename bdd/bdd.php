<?php

//Données BDD
require('bddData.php');

function connectToDb()
{
    //Connexion BDD
    try {
        $connectionDB = new PDO('mysql:host=localhost;dbname=bricobleau', USER, PASSWD);
        $connectionDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    return $connectionDB;
}

function getDataFor($valeurCategorie)
{
    $requeteSQL = "SELECT reference, designation, photo, prix, stock FROM produit WHERE categorie = ? ";

    $sth = connectToDb()->prepare($requeteSQL);
    $sth->execute(array($valeurCategorie));
    return $sth;
}

function getDataForAuthentification($login, $password)
{
    $requeteSQL = "SELECT identifiant , motDePasse FROM utilisateur WHERE identifiant = ? AND motDePasse = ?";

    $sth = connectToDb()->prepare($requeteSQL);
    $sth->execute(array($login, $password));
    return $sth;
}

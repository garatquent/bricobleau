SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

USE bricobleau;

INSERT INTO `produit` ( `reference`, `designation`, `photo` ,`prix`, `stock`, `categorie` ) VALUES
( 'sci01' , 'Scie égoïne JETCUT - Coupe fine - 500mm' , '../img/scie-jetcut-stanley.jpg' , 18.23 , 5 , 'coupe'),
( 'sci02' , 'Monture de scie à métaux Facom 603FPB' , '../img/scie-metaux-facom.jpg', 30.95 , 12 , 'coupe'),
( 'sci03' , 'Boite à onglet pour plinthes polystyrène + scie à araser' , '../img/scie-onglets.jpg', 22.95 , 9 , 'coupe'),
( 'sce01' , 'Scie circulaire MAKITA 235mm en coffret-5903RK' , '../img/scie-circulaire-makita.jpg', 253.48 , 5 , 'coupe'),
( 'sce02' , 'Scie à onglet radial DEWALT 1675W 305MM -DWS780' , '../img/scie-onglet-electrique.jpg', 954.4 , 2,  'coupe'),
( 'sce03' , 'Tronconneuse à metaux DEWALT lame à carbure 2200W 355MM -DW872' , '../img/tronconneuse-metaux.jpg', 777.64 , 64 , 'coupe'),
( 'mar01' , 'Marteau multifonctions IsoCore L' , '../img/marteau-multifonction.jpg', 72.67 , 24, 'frappe'),
( 'mar02' , 'Facom Marteau de mécanicien rivoir manche graphite, 270 mm 200C.30', '../img/facom-marteau-meca.jpg', 351.23 , 1 , 'frappe'),
( 'mar03' , 'WestFalia Marteau de charpentier -900g' , '../img/marteau-charpentier.jpg', 55.94 , 120, 'frappe'),
( 'mas01' , 'Fiskar Masse IsoCore L,92cm- 4.76kg -1020219' , '../img/massette-fiskaars.jpg', 76.99,  15, 'frappe'),
( 'mas02' , 'Massette Carré manche bi-matière  -Poids(g) 1000 -Stanley' , '../img/massette-stanley.jpg', 18.1 , 201 , 'frappe'),
( 'mas03' , 'Masse à tranche LEBORGNE avec Manche NovaMax' , '../img/masse-tranche.jpg', 48.4,  63 , 'frappe'),
( 'tou01' , 'Jeu de 6 Tournevis Protwist isolés 1000V FACOM -ATDVE.J6PB' , '../img/jeu-6-tournevis.jpg', 31.23,  111 , 'visse'),
( 'tou02' , 'Tournevis TORX ASSORTIMENT T6-7-8-9-10-15-20-25-27-30-40 Bergland BGS' , '../img/tournevis-torx.jpg', 11.85 , 85,  'visse'),
( 'tou03' , 'Jeu de clés à douille hexagonal 322 K5 5 pièces 5, 5-7,0-8,0-10,0-13,0' , '../img/jeu-cle-douille.jpg', 87.64 , 24 , 'visse'),
( 'cle01' , 'Jeu de 9 clés males longues 6 pans FACOM 83SH.JJPAAPB' , '../img/9-cles-6pans.jpg', 19.74 , 999 , 'visse'),
( 'cle02' , 'Jeu de 9 clés à pipe débouchées FACOM - 75.J9PB' , '../img/cles-à-pipe-facom.jpg', 68.7 , 305 , 'visse'),
( 'cle03' , 'Jeux de clés mixtes métriques FACOM étui portatif-14 pcs -440.JP14' , '../img/cles-mixtes-facom.jpg', 145.4 , 43 , 'visse');


INSERT INTO `utilisateur` (`nom` , `prenom` , `identifiant` , `motDePasse`) VALUES
('Lagarrue' , 'Alexandre' , 'lagarrueal' , '1234'),
('Garat' , 'Quentin' , 'garatquent' , '4567');

COMMIT;
<?
session_start();
include('varSession.inc.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js"></script>
    <title>BricoBleau</title>
</head>

<?php
$_SESSION['identifiant'] = null;
echo "<script type='text/javascript'>window.location.replace('../index.php');</script>";
?>

</html>
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js"></script>
    <title>BricoBleau</title>
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="titre">
                <img src="https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png" alt="police-supreme">
                <p id="description">- Votre magasin d'outillage à Fontainebleau -</p>

                <div class="menu">
                    <a href="../index.php">Accueil</a>
                    <a href="produits.php?cat=frappe">Outils pour frapper</a>
                    <a href="produits.php?cat=visse">Outils pour visser</a>
                    <a href="produits.php?cat=coupe">Outils pour découper</a>
                    <a id="actuelle" href="contact.php">Contact</a>
                    <a href="affichageFormInscription.php"> Inscription</a>
                </div>
            </div>
            <?php include('connexion.inc.php'); ?>
        </div>

        <?php
        if (!empty($_POST)) {
            if ((!empty($_POST['nom'])) && (!empty($_POST['prenom'])) && (!empty($_POST['mail'])) && (!empty($_POST['ddn'])) && (!empty($_POST['metier']))
                && (!empty($_POST['sujet'])) && (!empty($_POST['contenuMail']))
            ) {
                //$header.='From:"nom_d\'expediteur"<votre@mail.com>'."\n";
                //$header.='Content-Type:text/html; charset="uft-8"'."\n";
                //$header.='Content-Transfer-Encoding: 8bit';
                $messageMail = "
                    <html>
                        <body>
                            <div align='center'>
                                <img src='https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png'/>
                                <br />
                                <u>Nom de l\'expéditeur :</u>" . $_POST['nom'] . "<br />
                                <u>Mail de l\'expéditeur :</u>" . $_POST['mail'] . "<br />
                                <br />
                                " . nl2br($_POST['contenuMail']) . "
                                <br />
                                <img src='https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png'/>
                            </div>
                        </body>
                    </html>
                    ";
                $retourMail = mail("garatquent@eisti.eu", $_POST['sujet'], $messageMail);
                if ($retourMail) {
                    $msg = "Votre message a bien été envoyé !";
                } else {
                    $msg = "Erreur d'envoi du message";
                }
            } else {
                $msg = "Tous les champs doivent être complétés !";
            }
        }
        ?>

        <div class="contenu">
            <h2>Formulaire de contact</h2>
            <form action="contact.php" method="POST">
                <div>
                    <label for="nom">Nom :</label>
                    <input type="text" name="nom" id="nom" placeholder="Votre nom de famille">
                    <span id="sansNom"></span>
                </div>
                <div>
                    <label for="prenom">Prénom :</label>
                    <input type="text" name="prenom" id="prenom" placeholder="Votre prénom">
                    <span id="sansPrenom"></span>
                </div>
                <div>
                    <label for="mail">E-mail :</label>
                    <input type="email" name="mail" id="mail" placeholder="prenom.nom@gmail.com">
                    <span id="sansMail"></span>
                </div>
                <div>
                    <label for="ddn">Date de naissance :</label>
                    <input type="date" name="ddn" id="ddn">
                    <span id="sansDate"></span>
                </div>
                <div>
                    <label>Genre :</label>
                    <input type="radio" id="homme" name="genre" checked>
                    <label for="homme">Homme</label>
                    <input type="radio" id="femme" name="genre">
                    <label for="femme">Femme</label>
                    <input type="radio" id="autre" name="genre">
                    <label for="autre">Autre</label>
                </div>
                <div>
                    <label for="metier">Métier :</label>
                    <select id="metier" name="metier">
                        <option value="electricien">Electricien</option>
                        <option value="macon">Maçon</option>
                        <option value="carreleur">Carreleur</option>
                        <option value="ouvrier">Ouvrier</option>
                        <option value="mecanicien">Mécanicien</option>
                        <option value="menuisier">Menuisier</option>
                        <option value="particulier">Particulier</option>
                    </select>
                </div>
                <div>
                    <label for="sujet">Sujet :</label>
                    <input type="text" name="sujet" id="sujet" placeholder="Entrez le sujet de votre mail">
                    <span id="sansSujet"></span>
                </div>
                <div>
                    <label id="labelContenuMail" for="contenuMail">Contenu :</label>
                    <textarea name="contenuMail" id="contenuMail" cols="30" rows="5" placeholder="Entrez le contenu de votre mail"></textarea>
                    <span id="sansContenu"></span>
                </div>
                <div>
                    <input type="submit" id="bouton" value="Envoyer"></input>
                </div>
            </form>

            <?php
            if (isset($msg)) {
                echo $msg;
            }
            ?>
        </div>

        <?php include('footer.inc.php'); ?>
    </div>

</body>

</html>
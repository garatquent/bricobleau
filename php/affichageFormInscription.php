<?php
session_start();
include('varSession.inc.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js"></script>
    <title>BricoBleau</title>
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="titre">
                <img src="https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png" alt="police-supreme">
                <p id="description">- Votre magasin d'outillage à Fontainebleau -</p>

                <div class="menu">
                    <a href="../index.php">Accueil</a>
                    <a href="produits.php?cat=frappe">Outils pour frapper</a>
                    <a href="produits.php?cat=visse">Outils pour visser</a>
                    <a href="produits.php?cat=coupe">Outils pour découper</a>
                    <a href="contact.php">Contact</a>
                    <a id="actuelle" href="affichageFormInscription.php"> Inscription</a>
                </div>
            </div>
            <?php include('connexion.inc.php'); ?>
        </div>


        <div class="contenu">

            <form action="inscription.php" method="POST">
                <div>
                    <label for="nom">Nom:</label>
                    <input type="text" name="nom" id="nom" placeholder="nom">
                    <span id="sansNom"></span>
                </div>
                <div>
                    <label for="prenom">Prénom :</label>
                    <input type="text" name="prenom" id="prenom" placeholder="prenom">
                    <span id="sansPrenom"></span>
                </div>
                <div>
                    <label for="identifiant">Identifiant :</label>
                    <input type="text" name="identifiant" id="identifiant" placeholder="Identifiant">
                    <span id="sansIdentifiant"></span>
                </div>
                <div>
                    <label for="mot de passe">Mot de passe :</label>
                    <input type="text" name="motDePasse" id="mdp" placeholder="Mot de passe">
                    <span id="sansMDP"></span>
                </div>
                <br>
                <div>
                    <input type="submit" id="bouton" value="S'inscire"></input>
                </div>
            </form>

            <p>Vous avez déjà un compte ? <br>
                <a href="connexion.php"> Se connecter</a>
            </p>

        </div>
        
        <?php include('footer.inc.php'); ?>
    </div>

</body>

</html>
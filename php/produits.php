<?php
session_start();
include('varSession.inc.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js"></script>
    <title>BricoBleau</title>
</head>

<body onload="javascript:disap();">

    <div class="page">
        <div class="header">
            <div class="titre">
                <img src="https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png" alt="police-supreme">
                <p id="description">- Votre magasin d'outillage à Fontainebleau -</p>

                <div class="menu">
                    <a href="../index.php">Accueil</a>
                    <?php
                    switch ($_GET['cat']) {
                        case "visse":
                            echo "<a href='produits.php?cat=frappe'>Outils pour frapper</a> <a id='actuelle' href='produits.php?cat=visse'>Outils pour visser</a> <a href='produits.php?cat=coupe'>Outils pour découper</a>";
                            break;
                        case "coupe":
                            echo "<a href='produits.php?cat=frappe'>Outils pour frapper</a> <a href='produits.php?cat=visse'>Outils pour visser</a> <a id='actuelle' href='produits.php?cat=coupe'>Outils pour découper</a>";
                            break;
                        case "frappe":
                            echo "<a id='actuelle' href='produits.php?cat=frappe'>Outils pour frapper</a> <a href='produits.php?cat=visse'>Outils pour visser</a> <a href='produits.php?cat=coupe'>Outils pour découper</a>";
                            break;
                    }
                    ?>
                    <a href="contact.php">Contact</a>
                    <a href="affichageFormInscription.php"> Inscription</a>
                </div>
            </div>
            <?php include('connexion.inc.php'); ?>
        </div>
        <div class="contenu">
            <?php
            if (($_GET['cat'] != "visse") && ($_GET['cat'] != "frappe") && ($_GET['cat'] != "coupe")) {
                echo "Il y a une erreur dans la catégorie, veuillez revenir à l'index";
            } else {
                $tab = $_SESSION[$_GET['cat']];
                $categorie;
                switch ($_GET['cat']) {
                    case "visse":
                        echo "<h2>Outils pour visser</h2>";
                        $categorie = "visse";
                        break;
                    case "coupe":
                        echo "<h2>Outils pour découper</h2>";
                        $categorie = "coupe";
                        break;
                    case "frappe":
                        $categorie = "frappe";
                        echo "<h2>Outils pour frapper</h2>";
                        break;
                    default:
                        $categorie = "inconnue";
                }
            ?>
                <table>
                    <tr>
                        <td>Référence</td>
                        <td>Désignation</td>
                        <td>Photo</td>
                        <td>Prix</td>
                        <td class='qte'>Quantité en stock</td>
                        <td>Quantité commandée</td>
                    </tr>

                    <?php
                    try {
                        $dataCategorie = getDataFor($categorie);
                        $tabCategorie = array("reference", "designation", "photo", "prix", "stock");
                        $cptGrd = 0;
                        foreach ($dataCategorie as $row) {
                            echo "<tr>";
                            $cptGrd++;
                            for ($i = 0; $i < 5; $i++) {
                                switch ($i) {
                                    case 4:
                                        echo '<td class="qte">';
                                        echo $row[$tabCategorie[$i]];
                                        echo "</td>";
                                        break;
                                    case 2:
                                        echo "<td onmouseover=javascript:imageZoom('" . $categorie . $cptGrd . "'); onmouseout=javascript:imageDezoom('" . $categorie . $cptGrd . "'); >";
                                        echo "<img id='" . $categorie . $cptGrd .  "' src= '" . $row[$tabCategorie[$i]] . "' width='100' height='100' >";
                                        echo "</td>";
                                        break;
                                    default:
                                        echo "<td>";
                                        echo $row[$tabCategorie[$i]];
                                        echo "</td>";
                                        break;
                                }
                            }
                            echo "<td>";
                            echo "<div>";
                            echo "<button onclick=javascript:decrease('" . $row[$tabCategorie[0]] . "');>-</button><span id='" . $row[$tabCategorie[0]] . "'>0</span><button onclick=javascript:increase('" . $row[$tabCategorie[0]] . "'," . $cptGrd . ");>+</button>";
                            echo "</div>";
                            if ((empty($_SESSION['identifiant'])) || ($_SESSION['identifiant'] == null)) {
                                echo "<button>Connectez-vous pour commander</button>";
                            } else {
                                echo "<button onclick=javascript:ajoutPanier('" . $row[$tabCategorie[0]] . "');>Commander</button>";
                            }
                            echo "</td>";
                            echo "</tr>";
                            echo "</tr>";
                        }
                        echo '<script>javascript:changeDisplay();</script>';
                    } catch (Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
                    ?>

                </table>
                <button id="stk" onclick="javascript:changeDisplay()">Stock</button>
        </div>
    <?php
            }
            if (($_GET['cat'] != "visse") && ($_GET['cat'] != "frappe") && ($_GET['cat'] != "coupe")) {
                echo "</div>";
            }
            include('footer.inc.php');
    ?>
    </div>

</body>

</html>
<?php
session_start();
include('varSession.inc.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/script.js"></script>
    <title>BricoBleau</title>
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="titre">
                <img src="https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png" alt="police-supreme">
                <p id="description">- Votre magasin d'outillage à Fontainebleau -</p>

                <div class="menu">
                    <a href="../index.php">Accueil</a>
                    <a href="produits.php?cat=frappe">Outils pour frapper</a>
                    <a href="produits.php?cat=visse">Outils pour visser</a>
                    <a href="produits.php?cat=coupe">Outils pour découper</a>
                    <a href="contact.php">Contact</a>
                    <a id="actuelle" href="affichageFormInscription.php"> Inscription</a>
                </div>
            </div>
            <?php include('connexion.inc.php'); ?>
        </div>


        <?php
        $login = $_POST['identifiant'];
        $motDePasse = $_POST['motDePasse'];

        try {
            $dataAuthentification = getDataForAuthentification($login, $motDePasse);
            $tabAuthentification = array("identifiant", "motDePasse");
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        $authentificationCorrecte = FALSE;
        foreach ($dataAuthentification as $row) {
            $authentificationCorrecte = TRUE;
        }

        if (!empty($_POST)) {
            if ((empty($_POST['identifiant'])) || (empty($_POST['motDePasse']))) {
                $msg = "Tous les champs doivent être complétés !";
            } elseif ($authentificationCorrecte) {
                $_SESSION['identifiant'] = $login;
                $msg =  'Vous êtes connecté !';
            } else {
                $msg =  'Mauvais identifiant ou mot de passe !';
            }
        }
        ?>

        <div class="contenu">
            <form action="connexion.php" method="POST">
                <div>
                    <label for="identifiant">Identifiant :</label>
                    <input type="text" name="identifiant" id="identifiant" placeholder="Identifiant">
                    <span id="sansIdentifiant"></span>
                </div>
                <br>
                <div>
                    <label for="mot de passe">Mot de passe :</label>
                    <input type="text" name="motDePasse" id="mdp" placeholder="Mot de passe">
                    <span id="sansMDP"></span>
                </div>
                <br>
                <div>
                    <input type="submit" id="bouton" value="Se connecter"></input>
                </div>
            </form>

            <?php
            if (isset($msg)) {
                echo $msg;
            }
            ?>
        </div>
        <?php include('footer.inc.php'); ?>
    </div>



</body>

</html>
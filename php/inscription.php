<?php
session_start();
require('../bdd/bddData.php');


try {
    $connectionDB = new PDO('mysql:host=localhost;dbname=bricobleau', USER, PASSWD);
    $connectionDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e;
    exit();
}



if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['identifiant']) && !empty($_POST['motDePasse'])) {

    $stmt = $connectionDB->prepare("INSERT INTO utilisateur (nom, prenom , identifiant , motDePasse) VALUES (:nom, :prenom, :identifiant, :motDePasse)");
    $stmt->bindParam(':nom', $nom);
    $stmt->bindParam(':prenom', $prenom);
    $stmt->bindParam(':identifiant', $login);
    $stmt->bindParam(':motDePasse', $password);

    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $login = $_POST['identifiant'];
    $password = $_POST['motDePasse'];

    $stmt->execute();

    echo '<script>alert("Votre inscription est validée");</script>';
    header('Location: connexion.php');
    exit();
    
}else {
    
    echo '<script>alert("Veuillez remplir tout les champs");</script>';
    header('Location: affichageFormInscription.php');
    
    exit();
    
}


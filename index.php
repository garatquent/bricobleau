<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js"></script>
    <title>BricoBleau</title>
</head>

<body>
    <div class="page">
        <div class="header">
            <div class="titre">
                <img src="https://fontmeme.com/permalink/210219/2ae988dcf5dd8fe57231cd06967034d5.png" alt="police-supreme">
                <p id="description">- Votre magasin d'outillage à Fontainebleau -</p>

                <div class="menu">
                    <a id="actuelle" href="index.php">Accueil</a>
                    <a href="php/produits.php?cat=frappe">Outils pour frapper</a>
                    <a href="php/produits.php?cat=visse">Outils pour visser</a>
                    <a href="php/produits.php?cat=coupe">Outils pour découper</a>
                    <a href="php/contact.php">Contact</a>
                    <a href="php/affichageFormInscription.php"> Inscription</a>
                </div>
            </div>
            <?php include('php/connexion.inc.php'); ?>
        </div>

        <div class="contenu">
            <div class="presentation">
                <p><span> Bienvenue sur le site internet de l'enseigne BricoBleau, votre magasin spécialisé en bricolage à Fontainebleau !</span>
                    <br>
                    <br>
                    Vous trouverez sur notre site l'intégralité des produits disponibles à l'achat dans notre magasin, mais n'hésitez pas à vous rendre sur place
                    si vous souhaitez obtenir des informations complémentaires sur quelconques produits, nos experts sont là pour vous.
                </p>
            </div>

            <div class="horaires">
                <p>Notre magasin vous accueille :</p>
                <p>Du lundi au vendredi : 9h - 13h / 14h - 19h</p>
                <p>Le samedi : 9h - 19h</p>
            </div>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1642.944939299084!2d2.7288452447498064!3d48.417752958973324!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e5f44d1183bf0d%3A0x434c49c8dcda6a0d!2sVolvo%20Fontainebleau%20-%20Elys%C3%A9e%20Automobiles!5e1!3m2!1sfr!2sfr!4v1613745832609!5m2!1sfr!2sfr" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>

        <?php include('php/footer.inc.php'); ?>
    </div>

</body>

</html>